
var memory_array = ['A', 'A', 'B', 'B', 'C', 'C', 'D', 'D', 'E',
    'E', 'F', 'F', 'G', 'G', 'H', 'H', 'I', 'I', 'J', 'J', 'K', 'K', 'L', 'L'];
var memory_values = [];
var memory_tile_ids = [];
var tiles_flipped = 0;

var createImage = function (src, title) {
    var img = new Image();
    img.src = src;
    img.alt = title;
    img.title = title;
    return img;
};

// array of images
var images = ['./images/clouds.jpg', './images/clouds.jpg', './images/coins.jpg', './images/coins.jpg',
    './images/doggy.jpg', './images/doggy.jpg', './images/field.jpg', './images/field.jpg',
    './images/leaves.jpg', './images/leaves.jpg', './images/paper.jpg', './images/paper.jpg', './images/road.jpg',
    './images/road.jpg', './images/still.jpg', './images/still.jpg', './images/sunClouds.jpg', './images/sunClouds.jpg',
    './images/violetFlower.jpg', './images/violetFlower.jpg', './images/yellowFlower.jpg',
    './images/yellowFlower.jpg', './images/yellowStone.jpg', './images/yellowStone.jpg'
];
/*
// push two images to the array
images.push(createImage("./images/clouds.jpg", "clouds"));
images.push(createImage("./images/coins.jpg", "coins"));
images.push(createImage("./images/doggy.jpg", "doggy"));
images.push(createImage("./images/field.jpg", "field"));
images.push(createImage("./images/leaves.jpg", "leaves"));
images.push(createImage("./images/paper.jpg", "paper"));
images.push(createImage("./images/road.jpg", "road"));
images.push(createImage("./images/still.jpg", "still"));
images.push(createImage("./images/sunClouds.jpg", "sunCloud"));
images.push(createImage("./images/violet flower.jpg", "violetF"));
images.push(createImage("./images/yellow flower.jpg", "yellow flower"));
images.push(createImage("./images/yellowStone.jpg", "yellowStone"));
*/


Array.prototype.memory_tile_shuffle = function () {
    var i = this.length, j, temp;
    while (--i > 0) {
        j = Math.floor(Math.random() * (i + 1));
        temp = this[j];
        this[j] = this[i];
        this[i] = temp;
    }
}

function newBoard() {
    tiles_flipped = 0;
    var output = '';
    images.memory_tile_shuffle();
    for (var i = 0; i < images.length; i++) {
        output += '<div id="tile_' + i + '" onclick="memoryFlipTile(this,\'' + images[i] + '\')"></div>';
    }
    document.getElementById('memory-board').innerHTML = output;

}

function memoryFlipTile(tile, val) {
    if (tile.innerHTML == "" && memory_values.length < 2) {
        tile.style.background = '#FFF';
        tile.innerHTML = val;
        if (memory_values.length == 0) {
            memory_values.push(val);
            memory_tile_ids.push(tile.id);
        } else if (memory_values.length == 1) {
            memory_values.push(val);
            memory_tile_ids.push(tile.id);
            if (memory_values[0] == memory_values[1]) {
                tiles_flipped += 2;
                // Clear both arrays
                memory_values = [];
                memory_tile_ids = [];
                // Check to see if the whole board is cleared
                if (tiles_flipped == images.length) {
                    alert("Board cleared... generating new board");
                    document.getElementById('memory-board').innerHTML = "";
                    newBoard();
                }
            } else {
                function flip2Back() {
                    // Flip the 2 tiles back over
                    var tile_1 = document.getElementById(memory_tile_ids[0]);
                    var tile_2 = document.getElementById(memory_tile_ids[1]);
                    tile_1.style.background = 'url(./images/oie_29145426FXBLw7or.jpg) no-repeat';
                    tile_1.innerHTML = "";
                    tile_2.style.background = 'url(./images/oie_29145426FXBLw7or.jpg) no-repeat';
                    tile_2.innerHTML = "";
                    // Clear both arrays
                    memory_values = [];
                    memory_tile_ids = [];
                }
                setTimeout(flip2Back, 700);
            }
        }
    }
}